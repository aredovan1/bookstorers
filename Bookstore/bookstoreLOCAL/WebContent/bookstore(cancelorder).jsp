<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bookstore</title>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <link rel="stylesheet" type="text/css" href="css/html.css" media="screen, projection, tv " />
  <link rel="stylesheet" type="text/css" href="css/ies.css" media="screen, projection, tv " />


</head>
<body>
 <span class="grey" id="helloUser"></span>
<div>
 <div class="rounded" style="position: relative; height:100px;">
  <div>         
    <label for="Login">Login</label>
    <input type="text" name="Username" id="Username"/>
  </div>
  <div>
    <label for="password">Password</label>
    <input type="password" name="password" id="Password" class="txt" />
  </div>
   <div id="login_b">
      <!-- h1 class="clear"><a href="#" id="loginbutton">Log In</a></h1-->
      <input type="submit" value="Log In" class="button" id="loginbutton"/>
  </div>  
  
   <div class="hidden"><a href="#"  id="getAllOrders"><span class="green">Show all orders</span></a></div> 
</div>
<div class="boostore_logo" style="position: relative; height:100px; align:center">
  <h1><span class="big darkBrown">B</span>ookstore</h1>
</div>
</div>


 <div class="boostore_logo">
  <h1><span class="big darkBrown">B</span>ookstore</h1>
</div>

    <!-- form for autorization -->
		


<!-- #wrapper: centers the content and sets the width -->
<div id="wrapper">
 <!-- #content: applies the white, dropshadow background -->
  <div id="contentcss">
  <!-- div id="myDiv">id="myDiv"</div-->  
    <!-- #header: holds site title and subtitle -->
    <div id="header">
  <br/>    
<br/>  
<div id="page">



		<div class="table1" id="divbooksearch">
		<table cellspacing="0">
		<tr>
		<th>
		<select id="searchType">
		  <option>Author</option>
		  <option>Title</option>
		</select>
		</th> 
		<th><input id = "term" type="text" size="40"></th>
		<th><input type="submit" value="Search" class="button" id="searchbot"/></th>
		<th><!-- a href="#" class="button" id="searchbot">Search</a--></th>
		
		</tr>
		</table>
		
 
             <span id="getAllBooks"><span class="darkBrown1">Show all books</span></span>
       
		</div>
<p/>    
<p/> 
<div id="content">Content area</div>   
<div id="makeanorderdiv" class="makeanorderdiv">
<table>
<tr><td colspan=2>Billing Address</td></tr>
<tr><td> Address line1 	</td>
    <td><input id="biladdress1" type="text"></td>
</tr>
<tr><td> Address line2 	</td>
    <td><input id="biladdress2" type="text"></td>
</tr>
<tr><td>  City 	</td>
    <td><input id="bcity" type="text"></td>
</tr>
<tr><td> State 	</td>
 	<td><input id="bstate" type="text"></td>
</tr>
<tr><td> zip code </td>
 	<td><input id="bzip" type="text"></td>
</tr>
<tr><td>  Phone </td>
 	<td><input id="bphone" type="text"></td>
</tr>
<tr><td> Email </td>
 	<td><input id="bemail" type="text"></td>
</tr>
<tr><td colspan=2>Shipping  Address</td></tr>
<tr><td> Address line1 </td>
 	<td><input id="siladdress1" type="text"></td>
</tr>
<tr><td>  Address line2 </td>
 	<td><input id="siladdress2" type="text"></td>
</tr>
<tr><td> City </td>
 	<td><input id="scity" type="text"></td>
</tr>
<tr><td>  State </td>
 	<td><input id="sstate" type="text"></td>
</tr>
<tr><td>  zip code </td>
 	<td><input id="szip" type="text"></td>
</tr>
<tr><td> Phone</td>
 	<td><input id="sphone" type="text"></td>
</tr>
<tr><td> Email </td>
 	<td><input id="semail" type="text"></td>
<tr><td colspan=2> Payment Information</td></tr>
<tr><td> Cardholder Name  </td>
 	<td><input id="cardholderName" type="text"></td>
</tr>
<tr><td> Credit Card Number </td>
 	<td><input id="creditCardNumber" type="text"></td>
</tr>
<tr><td> Expiration Date </td>
 	<td><input id="expirationDate" type="text"></td>
</tr>
<tr><td> Verification Code   </td>
 	<td> <input id="verificationCode" type="text"></td>
</tr>
</table> 

<input type="submit" value="Place an order" class="button" id="createAnOrderButton"/>
<!-- a href="#" class="button" id="createAnOrderButton">Place an order</a-->
</div> 
    
    
    </div>
  </div>
</div>
</div>    
			
<script>
$(document).ready(function () {	
		
/////////////////////////////****************************************/////////////////////////////		
// ----------- make authorization 
    // Username and Password
    function getUsername() {
        return $("#Username").val();
    }

    function getPassword() {
        return $("#Password").val();
    }

    function getAuthParams() {
        return "username=" + getUsername() + "&password=" + getPassword();
    } 
    
 // loging user
 // get customer information for authorization
var loginUser = function (){
 //alert(getAuthParams());
 $.getJSON('http://localhost:8080/bookservice/customer/' + getUsername(), function () {
	   authorizeUser(customers);
 	 })
    }
 
     //-------------------- autorization	--------------------//	 
     
     
var authorizeUser = function (customers) {          
     for (var i = 0; i < customers.length; i++) {
    	 // checking if passord is correct we make order information available
    	if (customers.password==getPassword())
    	  {
    		   $('#helloUser').html('Welcome, '+getUsername());
    		   $('div.hidden').show();
    	   }
    	  else    
    	   {
    		 alert("Password is incorrect");
    	   }           
  }     	          
}     

/////////////////////////////******************SEARCH BOOK*********************///////////////////////////// 

//-------------------- Serch by title or by author	--------------------//
function SearchOptionVals() {
	var valueType = $("#searchType").val();
		        if (valueType=="Author") {var URIpart="author";} //variable needed to build the path
		        if (valueType=="Title") {var URIpart="title";}   //variable needed to build the path
// Get the search term.
 var bookSearch = $('#term').val();
// If the title was empty, give an error message.
 if (bookSearch == '') {
	$('#content').html('No search term.');
// Otherwise do API call
	 } else {
	 $('#content').html("Search results...");
	   // we will be searching by title or author 
	   // if author was choosen, we go for book/author otherwise book/title
	     $.getJSON('http://localhost:8080/bookservice/book/'+URIpart+'/' + bookSearch, function (books) {
		                produceBookList(books);
		            });
  }
}
	 
//GET ALL BOOKS
 var getAllBooks = function () {
     $.getJSON('http://localhost:8080/bookservice/book/', function (books) {
         produceBookList(books);
     });
}



//-------------------- Produce a list of books	--------------------//	
// constructin list with books, after user click on any book getBookInfo is called
var produceBookList = function (books) {
//var content = '<ul><li class="bookInfo" id=123>' + 'books 123' + '</li>';
   for (var i = 0; i < books.length; i++) {
           content += '<li class="bookInfo" id="' + books[i].id + '">' + books[i].title + ', by ' + books[i].author + '</li>';
         }
	       content += '</ul>';
           $('#content').html(content);
	    }      
 
 
//////////////////////////**************ORDERS*************////////////////////////  

//GET ALL ORDERS of this user
 var getAllOrders = function (){
	 $.getJSON('http://localhost:8080/bookservice/order?' + getAuthParams(), function (orders) {
	       produceOrderList(orders);
	 })
 }
	
//constructin list of orders, after user click on any order getOrderInfo is called
// Produce a list of orders
	    var produceOrderList = function (orders) {
	        var content = '<ul>';
	        for (var i = 0; i < orders.length; i++) {
	            content += '<li class="orderInfo"><span class="link" id="' + orders[i].id + '">Order #' + orders[i].id + ': ' + orders[i].customer.firstName + ' ' + orders[i].customer.lastName + '</span> ';
	            for (var j = 0; j < orders[i].link.length; j++) {
	                content += '<span class="action" id="' + orders[i].id + '">' + orders[i].orderStatus + '</span> ';
	            }
	            content += '</li>';
	        }
	        content += '</ul>';
	        $('#content').html(content);
	    }
	    

 	  
 ////////////////////////////////////////////////////////////////////////////////////////////
 // GET BOOK INFORMATION
 //
	    var getBookInfo = function () {
	        $.getJSON('http://localhost:8080/bookservice/book/' + $(this).attr('id'), function (book) {
	            var content = 'ISBN: ' + book.isbn + '<br />';
	            content += 'Title: ' + book.title + '<br />';
	            content += 'Author: ' + book.author + '<br />';
	            content += 'Price: ' + book.price + '<br />';
	            content += '<span class="darkBrown1"><span id="buyButton" class="label label-info" rel="' + book.link[0].url + '">' + book.link[0].action + '</span></span> ';
	            $('#content').html(content);
	        });
	    }
 
 
 /////////////////////////////************Create an order********/////////////////////////////////////////
// BUY A BOOK
//creating an order
var buyBook = function () {
            //show form of mking an order
           // alert("111");
             $('#makeanorderdiv').show();	        
	     }

 // placing an order
var createAnOrder = function () {
	//alert("111");
	$('#makeanorderdiv').hide();	
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'type': 'POST',
        'url': $(this).attr('rel') + '&' + getAuthParams(),
        'success': function () {
            $('#content').html('Congradulations! Your order was placed.');
        }
    });
}  	     	    
	    
	    ///////////////////////////////////////////////////////////////////////////////////////////////
	    // GET ORDER INFORMATION
	    //
	    var getOrderInfo = function () {
	        $.getJSON('http://localhost:8080/bookservice/order/' + $(this).attr('id') + '?' + getAuthParams(), function (order) {
	            var content = 'Order #' + order.id + ' placed by ';
	            content += order.customer.firstName + ' ' + order.customer.lastName + '<br />';
	            content += order.orderState + '<br />';

	            content += '<h4>Billing Address</h4>';
	            content += order.billingAddress.firstName + ' ';
	            content += order.billingAddress.lastName + '<br />';
	            content += order.billingAddress.address1 + '<br />';
	            content += order.billingAddress.address2 + '<br />';
	            content += order.billingAddress.city + '<br />';
	            content += order.billingAddress.state + '<br />';
	            content += order.billingAddress.zip + '<br />';
	            content += order.billingAddress.phone + '<br />';
	            content += order.billingAddress.email + '<br />';

	            content += '<h4>Shipping Address</h4>';
	            content += order.shippingAddress.firstName + ' ';
	            content += order.shippingAddress.lastName + '<br />';
	            content += order.shippingAddress.address1 + '<br />';
	            content += order.shippingAddress.address2 + '<br />';
	            content += order.shippingAddress.city + '<br />';
	            content += order.shippingAddress.state + '<br />';
	            content += order.shippingAddress.zip + '<br />';
	            content += order.shippingAddress.phone + '<br />';
	            content += order.shippingAddress.email + '<br />';

	            content += '<h4>Credit Card</h4>';
	            content += order.creditCard.cardholderName + '<br />';
	            content += order.creditCard.creditCardNumber + '<br />';
	            content += order.creditCard.expirationDate + '<br />';
	            content += order.creditCard.verificationCode + '<br />';


	            content += '<h4>Books</h4>';
	            for (var i = 0; i < order.lines.length; i++) {
	                content += order.lines[i].book.title + ', by ' + order.lines[i].book.author + ' ';
	                content += order.lines[i].book.price + '<br />';
	            }

	            $('#content').html(content);
	        });
	    }

	    
	    
//////////////////////////////////////////////////////////////////////////////////////////////
 // PERFORM AN ORDER ACTION
  //
var performAction = function () {
	 var link = null;
	 var action = $(this).text();
	 var action_taken = null;

$.getJSON('http://localhost:8080/bookservice/order/' + $(this).attr('id') + '?' + getAuthParams(), function (order) {
switch (action) {

        case 'cancel':
	    order.orderState = true;
	    action_taken = "Canceled";
	    break;
	 }

 for (var i = 0; i < order.link.length; i++) {
      if (order.link[i].action == action) {
             link = order.link[i].url;
           }
     }

 if (link == null) {
      $('#content').html("Error proecessing request because the link is null.");
 } else {

	                $.ajax({
	                    headers: {
	                        'Accept': 'application/json',
	                        'Content-Type': 'application/json'
	                    },
	                    'type': 'POST',
	                    'url': link + '?' + getAuthParams(),
	                    'data': JSON.stringify(order),
	                    'dataType': 'json',
	                    'success': function () {
	                        $('#content').html('Your order has been ' + action_taken + '.');
	                    }
	                });
	            }

	        });

	    }
	    

 
// REGISTER BOOK INFO BUTTON
// $('li.bookInfo').live(''click, getBookInfo);
$('li.bookInfo').on('click', getBookInfo);
	    
//REGISTER BOOK INFO BUTTON
//$('.bookInfoLi').on('click', getBookInfo);
//$('li.bookInfo').click(function(){alert( "hi!" ));
//$(document).on("click", ".bookInfoLi", getBookInfo); 

$(document).on("click", "li.bookInfo", getBookInfo)

//REGISTER ORDER INFO BUTTON
$(document).on("click", "li.orderInfo .link", getOrderInfo);


//REGISTER BUY BUTTONS
//$('#buyButton').on('click', buyBook);
$('#buyButton').click(buyBook);	
$(document).on("click", "#buyButton", buyBook);

//REGISTER place an order BUTTONS
//$('#createAnOrderButton').on('click', createAnOrder);
$(document).on("click", "#createAnOrderButton", createAnOrder);
//$('#createAnOrderButton').click(createAnOrder);	    


//REGISTER ACTION BUTTONS
$(document).on("click", "li.orderInfo .action", performAction);


//autorization button 
$('#loginbutton').click(loginUser);

//REGISTER GET ALL ORDERS BUTTON
$('#getAllOrders').click(getAllOrders);

//REGISTER GET ALL BOOKS BUTTON
$('#getAllBooks').click(getAllBooks);

//REGISTER SEARCH BUTTON
$('#searchbot').click(SearchOptionVals); 


});
</script>

</body>
</html>
