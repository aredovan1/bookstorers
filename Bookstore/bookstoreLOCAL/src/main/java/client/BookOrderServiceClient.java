package main.java.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Response;

import main.java.bookstore.Book;
import main.java.bookstore.Customer;
import main.java.services.representation.BookRequest;
import main.java.services.representation.OrderRequest;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;


public class BookOrderServiceClient {
	
	private BookOrderServiceClient() {
	} 

	public static void main(String args[]) throws Exception {

		List<Object> providers = new ArrayList<Object>();
		JacksonJsonProvider provider = new JacksonJsonProvider();
		provider.addUntouchable(Response.class);
		providers.add(provider);

		/*****************************************************************************************
		 * GET METHOD invoke
		 *****************************************************************************************/
		/*GET BOOK BY TITLE*/
		System.out.println("GET METHOD .........................................................");
		WebClient getClient = WebClient.create("http://localhost:8081/bookstoreLOCAL", providers);
		WebClient getClient2 = WebClient.create("http://localhost:8081/bookstoreLOCAL", providers);
		WebClient getClient3 = WebClient.create("http://localhost:8081/bookstoreLOCAL", providers);

		//Configuring the CXF logging interceptor for the outgoing message
		WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
		//Configuring the CXF logging interceptor for the incoming response
		WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());

		// change application/xml  to get in xml format
		getClient = getClient.accept("application/xml").type("application/xml").path("/services/books?request=title&value=Rant");

		//The following lines are to show how to log messages without the CXF interceptors
		String getRequestURI = getClient.getCurrentURI().toString();
		System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
		String getRequestHeaders = getClient.getHeaders().toString();
		System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);

		//to see as raw XML/json
		String response = getClient.get(String.class);
		System.out.println("GET METHOD Response: ...." + response +"\n");
		
		/*GET BOOK BY AUTHOR*/
		// change application/xml  to get in xml format
		getClient2 = getClient2.accept("application/json").type("application/json").path("/services/books?request=author&value=Chuck");

		//The following lines are to show how to log messages without the CXF interceptors
		getRequestURI = getClient2.getCurrentURI().toString();
		System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
		getRequestHeaders = getClient2.getHeaders().toString();
		System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);

		//to see as raw XML/json
		String response2 = getClient2.get(String.class);
		System.out.println("GET METHOD Response: ...." + response2 +"\n");
//		
//		/*GET ORDER BY ORDERNUMBER*/
//		// change application/xml  to get in xml format
//		getClient3 = getClient3.accept("application/json").type("application/json").path("/services/order/#1111");
//
//		//The following lines are to show how to log messages without the CXF interceptors
//		getRequestURI = getClient3.getCurrentURI().toString();
//		System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
//		getRequestHeaders = getClient3.getHeaders().toString();
//		System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);
//
//		//to see as raw XML/json
//		String response3 = getClient3.get(String.class);
//		System.out.println("GET METHOD Response: ...." + response3 +"\n");

		/*****************************************************************************************
		 * POST METHOD invoke
		 *****************************************************************************************/
		/*ADD BOOK*/
		System.out.println("POST METHOD .........................................................");
		WebClient postClient = WebClient.create("http://localhost:8081/bookstoreLOCAL", providers);
		WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
		WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());

		// change application/xml  to application/json get in json format
		postClient = postClient.accept("application/xml").type("application/json").path("/services/books/add");

		String postRequestURI = postClient.getCurrentURI().toString();
		System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
		String postRequestHeaders = postClient.getHeaders().toString();
		System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
		
		BookRequest bookRequest = new BookRequest();
		bookRequest.setBookAuthor("Chuck-Palahnuik");
		bookRequest.setBookTitle("Fight-Club");
		bookRequest.setBookPrice("$17.00");
		
		String responsePost =  postClient.post(bookRequest, String.class);
		System.out.println("POST METHOD Response ........." + responsePost + "\n");
		
		/*CREATE ORDER*/
		// change application/xml  to application/json get in json format
//		postClient = postClient.accept("application/json").type("application/xml").path("/services/order");
//
//		postRequestURI = postClient2.getCurrentURI().toString();
//		System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
//		postRequestHeaders = postClient2.getHeaders().toString();
//		System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
//		OrderRequest orderRequest = new OrderRequest();
//		Book book = new Book();
//		Customer customer = new Customer();
//			book.setBookTitle("Rant");
//			book.setBookAuthor("Chuck-Palahniuk");
//			book.setBookPrice("$15.00");
//		orderRequest.setBookOrderInfo(book);
//			customer.setFirstName("Jinno");
//			customer.setLastName("Redovan");
//			customer.setShippingAddress("123 Fake Street");
//			customer.setShippingState("IL");
//			customer.setShippingZip("60647");
//		orderRequest.setCustomer(customer);
////		orderRequest.setPaymentInfo("");
//
//		String responsePost2 =  postClient2.post(orderRequest, String.class);
//		System.out.println("POST METHOD Response ........." + responsePost2 + "\n");
//		
////		/*****************************************************************************************
////		 * PUT METHOD invoke
////		 *****************************************************************************************/
////		System.out.println("PUT METHOD .........................................................");
////		WebClient putClient = WebClient.create("http://localhost:8082", providers);
////		WebClient.getConfig(putClient).getOutInterceptors().add(new LoggingOutInterceptor());
////		WebClient.getConfig(putClient).getInInterceptors().add(new LoggingInInterceptor());
////		
////		// change application/xml  to application/json get in json format
////		putClient = putClient.accept("application/xml").type("application/json").path("/bookservice/order/#1111");
////		
////		String putRequestURI = putClient.getCurrentURI().toString();
////		System.out.println("Client PUT METHOD Request URI:  " + putRequestURI);
////		String putRequestHeaders = putClient.getHeaders().toString();
////		System.out.println("Client PUT METHOD Request Headers:  " + putRequestHeaders);
////		
////		OrderRequest orderRequest2 = new OrderRequest();
////		Payment payment = new Payment();
////		payment.setBillingAddress(customer.getShippingAddress());
////		payment.setBillingState(customer.getShippingState());
////		payment.setBillingZip(customer.getShippingZip());
////		payment.setCardName("Jinno Redovan");
////		payment.setCardNumber("1273818237198");
////		payment.setCsv("748");
////		payment.setExpiration("10/23");
////		payment.setPaymentType("Visa");
////		orderRequest2.setPayment(payment);
////		orderRequest2.setOrderNum("#1111");
////		
////		String responsePut =  putClient.put(orderRequest2, String.class);
////		System.out.println("PUT METHOD Response ........." + responsePut + "\n");
//		
//		/*****************************************************************************************
//		 * DELETE METHOD invoke
//		 *****************************************************************************************/
//		System.out.println("DELETE METHOD .........................................................");
//		WebClient deleteClient = WebClient.create("http://localhost:8082", providers);
//		WebClient.getConfig(deleteClient).getOutInterceptors().add(new LoggingOutInterceptor());
//		WebClient.getConfig(deleteClient).getInInterceptors().add(new LoggingInInterceptor());
//
//		// change application/xml  to application/json get in json format
//		deleteClient = deleteClient.accept("application/json").type("application/xml").path("/services/order/#1111");
//
//		String deleteRequestURI = deleteClient.getCurrentURI().toString();
//		System.out.println("Client DELETE METHOD Request URI:  " + deleteRequestURI);
//		String deleteRequestHeaders = deleteClient.getHeaders().toString();
//		System.out.println("Client DELETE METHOD Request Headers:  " + deleteRequestHeaders);
//
//		deleteClient.delete();
//		System.out.println("DELETE METHOD Response ......... OK");

		System.exit(0);
	}
}
