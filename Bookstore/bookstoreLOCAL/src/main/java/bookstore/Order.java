package main.java.bookstore;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import main.java.bookstore.Book;
import main.java.bookstore.Customer;
import main.java.bookstore.Payment;

@XmlRootElement()
public class Order implements Serializable{

	private static final long serialVersionUID = 1L;
	Payment payment;
	Customer customer;
	String orderNum = "";
	String orderStatus;
	Book book;
	
	public Order(){}

	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public void setBookOrderInfo(Book book) {
		this.book = book;
	}
	public Book getBookOrderInfo() {
		return book;
	}
	
}
