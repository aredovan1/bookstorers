package main.java.bookstore;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import main.java.bookstore.Book;
import main.java.bookstore.Customer;
import main.java.bookstore.Order;
import main.java.bookstore.Payment;

public class CompositeDAO {
	private static Set<Book> books = new HashSet<Book>();
	private static Set<Order> orders = new HashSet<Order>();
	private static Set<Customer> customers = new HashSet<Customer>();
	
	public CompositeDAO(){
		Book book = new Book();
		Book book1 = new Book();
		Book book2 = new Book();
		Book book3 = new Book();
		
		book.setBookId("0000");
		book.setBookAuthor("Chuck Palahniuk");
		book.setBookTitle("Rant");
		book.setBookPrice("$15.00");
		books.add(book);
		
		book1.setBookId("0001");
		book1.setBookAuthor("Chuck Palahniuk");
		book1.setBookTitle("Fight Club");
		book1.setBookPrice("$12.00");
		books.add(book1);
		
		book2.setBookId("0002");
		book2.setBookAuthor("Bram Stoker");
		book2.setBookTitle("Dracula");
		book2.setBookPrice("$5.00");
		books.add(book2);
		
		book3.setBookId("0003");
		book3.setBookAuthor("Neil Gaiman");
		book3.setBookTitle("American Gods");
		book3.setBookPrice("$5.00");
		books.add(book3);
		
		Order order = new Order();
		Payment payment = new Payment();
		Customer customer = new Customer();
		
		order.setBookOrderInfo(book);
		order.setOrderNum("1111");
		order.setOrderStatus("Processing...");
		//set customer/shipping information
			customer.setCustomerId("r1234");
			customer.setFirstName("Jinno");
			customer.setLastName("Redovan");
			customer.setShippingAddress("123 Fake Street");
			customer.setShippingState("IL");
			customer.setShippingZip("60647");
		customers.add(customer);
		order.setCustomer(customer);
		//set billing information
			payment.setBillingAddress("123 Fake Street");
			payment.setBillingState("IL");
			payment.setBillingZip("60647");
			payment.setCardName("Jinno Redovan");
			payment.setCardNumber("1273818237198");
			payment.setCsv("748");
			payment.setExpiration("10/23");
			payment.setPaymentType("Visa");
		order.setPayment(payment);
		
		orders.add(order);
	}
	
	public Set<Book> getAllBooks( ) {
		return books;
	}
	
	public Set<Order> getAllOrders( ) {
		return orders;
	}
	
	public Customer getCustomerById(String customerId) {
		Iterator<Customer> it = customers.iterator();
		while(it.hasNext()) {
			Customer customer = (Customer)it.next();
			if(customer.getCustomerId().equals(customerId)) {
				return customer;
			}
		}
		return null;
	}
	
	public Book getBookById(String bookId) {
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookId().equals(bookId)) {
				return book;
			}
		}
		return null;
	}
	
	public Book getBookByTitle(String bookTitle) {
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookTitle().contains(bookTitle)) {
				return book;
			}
		}
		return null;
	}
	
	public Book getBookByAuthor(String bookAuthor) {
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookAuthor().contains(bookAuthor)) {
				return book;
			}
		}
		return null;
	}
	
	public Book addBook(String bookTitle, String bookAuthor, String bookPrice) {
		Book book = new Book();
		
		Random random = new Random();
		int randomInt = random.nextInt(10000);
		String id = "#"+randomInt;
		
		book.setBookId(id);
		book.setBookAuthor(bookAuthor);
		book.setBookTitle(bookTitle);
		book.setBookPrice(bookPrice);
		
		books.add(book);
		
		return book;
	}
	
	public Order createOrder(Book buyBook, Customer customer, Payment payment) {
		Order order = new Order();
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookTitle().equals(buyBook.getBookTitle())) {
				
				Random random = new Random();
				int randomInt = random.nextInt(10000);
				String orderNum = "#"+randomInt;
				
				order.setOrderNum(orderNum);
				order.setOrderStatus("PROCESSING");
				order.setPayment(payment);
				order.setCustomer(customer);
				order.setBookOrderInfo(book);
				orders.add(order);
				return order;
			}
		}
		return null;
	}
	
	public Order getOrder(String orderNumber) {
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = (Order)it.next();
			if (order.getOrderNum().equals(orderNumber)) {
				return order;
			}
		}
		return null;
	}
	
	public void updateOrder(String orderNum, Payment payment) {
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = (Order)it.next();
			if(order.getOrderNum().equals(orderNum)) {
				order.setPayment(payment);
				order.setOrderStatus("SHIPPED");
				return;
			}
		}
	}
	
	public void deleteOrder(String orderNum) {
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = (Order)it.next();
			if(order.getOrderNum().equals(orderNum)) {
				orders.remove(order);
				return;
			}
		}
	}
}
