package main.java.services.workflow;

import main.java.bookstore.CompositeDAO;
import main.java.bookstore.Customer;
import main.java.bookstore.domain.model.Link;
import main.java.services.representation.CustomerRepresentation;

public class CustomerActivity {

	CompositeDAO dao = new CompositeDAO();
	
	public CustomerRepresentation getCustomer(String customerId) {
		
		Customer customer = dao.getCustomerById(customerId);
		CustomerRepresentation custRep = new CustomerRepresentation();
		custRep.setCustomerId(customer.getCustomerId());
		custRep.setFirstName(customer.getFirstName());
		custRep.setLastName(customer.getLastName());
		custRep.setShippingAddress(customer.getShippingAddress());
		custRep.setShippingState(customer.getShippingState());
		custRep.setShippingZip(customer.getShippingZip());
		
		setLinks(custRep, "id");
		
		return custRep;
	}

	public void setLinks(CustomerRepresentation customerRep, String action) {
			
			// create link with action passed in
			if (action.equals("id")) {
				Link request = new Link(action, "http://bookstorers.herokuapp.com/services/customers?id="+customerRep.getCustomerId());
				customerRep.setLinks(request);
			}
	}
	
	
}
