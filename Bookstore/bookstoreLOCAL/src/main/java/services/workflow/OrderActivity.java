package main.java.services.workflow;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import main.java.bookstore.CompositeDAO;
import main.java.bookstore.Order;
import main.java.bookstore.Payment;
import main.java.bookstore.domain.model.Link;
import main.java.services.representation.OrderRepresentation;
import main.java.services.representation.OrderRequest;

public class OrderActivity {
	CompositeDAO dao = new CompositeDAO();
	
	public Set<OrderRepresentation> getOrders() {
		Set<Order> orders = new HashSet<Order>();
		Set<OrderRepresentation> orderRepresentations = new HashSet<OrderRepresentation>();
		orders = dao.getAllOrders(); //@TODO
		
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = new Order();
			OrderRepresentation orderRep = new OrderRepresentation();
			orderRep.setBookOrderInfo(order.getBookOrderInfo());
			orderRep.setCustomer(order.getCustomer());
			orderRep.setOrderNum(order.getOrderNum());
			orderRep.setOrderStatus(order.getOrderStatus());
			orderRep.setPayment(order.getPayment());
			
			orderRepresentations.add(orderRep);
			setLinks(orderRep, "all");
		}
		
		return orderRepresentations;		
	}
	
	public OrderRepresentation createOrder(OrderRequest orderRequest) {
		Order order = dao.createOrder(orderRequest.getBookOrderInfo(), orderRequest.getCustomer(), orderRequest.getPayment());
		
		OrderRepresentation orderRep = new OrderRepresentation();
		orderRep.setBookOrderInfo(order.getBookOrderInfo());
		orderRep.setOrderNum(order.getOrderNum());
		orderRep.setOrderStatus(order.getOrderStatus());
		orderRep.setCustomer(order.getCustomer());
		orderRep.setPayment(order.getPayment());
		
		setLinks(orderRep, "buy");
		
		return orderRep;
	}
	
	public OrderRepresentation getOrder(String orderNumber) {
		Order order = dao.getOrder(orderNumber);
		
		OrderRepresentation orderRep = new OrderRepresentation();
		orderRep.setBookOrderInfo(order.getBookOrderInfo());
		orderRep.setOrderNum(orderNumber);
		orderRep.setOrderStatus(order.getOrderStatus());
		orderRep.setCustomer(order.getCustomer());
		orderRep.setPayment(order.getPayment());
		
		setLinks(orderRep, "get");
		
		return orderRep;
	}
	
	public String deleteOrder(String orderNumber) {
		
		dao.deleteOrder(orderNumber);
		
		return "ORDER CANCELLED";
	}
	
	public String updateOrder(String orderNumber, Payment payment) {
		
		dao.updateOrder(orderNumber, payment);
		
		return "STATUS UPDATED";
		
	}

	public void setLinks(OrderRepresentation orderRep, String action) {
		
		// create link with action passed in
		if (action.equals("all")) {
			Link request = new Link(action, "http://bookstorers.herokuapp.com/orderservice/order");
			orderRep.setLinks(request);
		}
	}
}
