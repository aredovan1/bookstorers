package main.java.services.workflow;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import main.java.bookstore.Book;
import main.java.bookstore.CompositeDAO;
import main.java.bookstore.domain.model.Link;
import main.java.services.representation.BookRepresentation;

public class BookActivity {
	
	CompositeDAO dao = new CompositeDAO();
	
	public Set<BookRepresentation> getBooks() {
		Set<Book> books = new HashSet<Book>();
		Set<BookRepresentation> bookRepresentations = new HashSet<BookRepresentation>();
		books = dao.getAllBooks();
		
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = new Book();
			BookRepresentation bookRep = new BookRepresentation();
			bookRep.setBookAuthor(book.getBookAuthor());
			bookRep.setBookId(book.getBookId());
			bookRep.setBookPrice(book.getBookPrice());
			bookRep.setBookTitle(book.getBookTitle());
			
			bookRepresentations.add(bookRep);
			setLinks(bookRep, "all");
		}
		
		return bookRepresentations;		
	}
	
	public BookRepresentation getBookById(String bookId) {
		
		Book book = dao.getBookById(bookId);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		setLinks(bookRep, "id");
		
		return bookRep;
	}
	
	public BookRepresentation getBookByTitle(String bookTitle) {
		
		Book book = dao.getBookByTitle(bookTitle);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		setLinks(bookRep, "title");
		
		return bookRep;
	}
	
	public BookRepresentation getBookByAuthor(String bookAuthor) {
		
		Book book = dao.getBookByAuthor(bookAuthor);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		setLinks(bookRep, "author");
		
		return bookRep;
	}
	
	public BookRepresentation addBook(String bookTitle, String bookAuthor, String bookPrice) {
		
		Book book = dao.addBook(bookTitle, bookAuthor, bookPrice);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		setLinks(bookRep, "add");
		
		return bookRep;
	}	
	
	public void setLinks(BookRepresentation bookRep, String action) {
		
		Link request = new Link(action, "http://bookstorers.herokuapp.com/services/books?request=id&value="	
				+bookRep.getBookId());
		
	}
	
}
