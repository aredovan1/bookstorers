package main.java.services;

import java.util.Set;

import javax.jws.WebService;
import javax.ws.rs.core.Response;

import main.java.services.representation.BookRepresentation;
import main.java.services.representation.BookRequest;
import main.java.services.representation.CustomerRepresentation;
import main.java.services.representation.OrderRepresentation;
import main.java.services.representation.OrderRequest;

@WebService
public interface Services {
	
	public BookRepresentation addBook(BookRequest bookRequest);
	public BookRepresentation getBookBy(String request, String value);
	public Set<BookRepresentation> getBooks();
	
	public CustomerRepresentation getCustomer(String customerId);
	
	public OrderRepresentation getOrder(String orderNumber);
	public OrderRepresentation createOrder(OrderRequest orderRequest);
	
	public Response updateOrder(OrderRequest orderRequest);
	public Response deleteOrder(String orderNumber);
	
	public Set<OrderRepresentation> getOrders();
	
	
}
