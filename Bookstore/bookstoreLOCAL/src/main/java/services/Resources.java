package main.java.services;

import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import main.java.services.representation.BookRepresentation;
import main.java.services.representation.BookRequest;
import main.java.services.representation.CustomerRepresentation;
import main.java.services.representation.OrderRepresentation;
import main.java.services.representation.OrderRequest;
import main.java.services.workflow.BookActivity;
import main.java.services.workflow.CustomerActivity;
import main.java.services.workflow.OrderActivity;

@Path("/")
public class Resources implements Services{


	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("books/add")
	public BookRepresentation addBook(BookRequest bookRequest) {
		System.out.println("POST METHOD Request from Client with ............." 
				+ bookRequest.getBookTitle() + " by: " + bookRequest.getBookAuthor());
		BookActivity bookActivity = new BookActivity();
		return bookActivity.addBook(bookRequest.getBookTitle(), bookRequest.getBookAuthor(), bookRequest.getBookPrice());
	}
	
	/*ID, Title, Author Requests*/
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("books")
	public BookRepresentation getBookBy(@QueryParam("request") String request,
										@QueryParam("value") String value) {
		
		BookActivity bookActivity = new BookActivity();
		
		if (request.equals("title")) {
			return bookActivity.getBookByTitle(value);
		} else if (request.equals("author")) {
			return bookActivity.getBookByAuthor(value);
		} else if (request.equals("id")) {
			return bookActivity.getBookById(value);
		}
		
		return null;
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("books/all")
	public Set<BookRepresentation> getBooks() {
		BookActivity bookActivity = new BookActivity();
		return bookActivity.getBooks();
	}
	
	/*CUSTOMERS*/
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("customers")
	public CustomerRepresentation getCustomer(@QueryParam("id") String id) {
		CustomerActivity customerActivity = new CustomerActivity();
		return customerActivity.getCustomer(id);
	}
	
	/*ORDERS*/
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("books/{orders")
	public OrderRepresentation getOrder(@QueryParam("ordernumber") String number) {
		OrderActivity orderActivity = new OrderActivity();
		return orderActivity.getOrder(number);
	}

	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("orders")
	public OrderRepresentation createOrder(OrderRequest orderRequest) {
		OrderActivity orderActivity = new OrderActivity();
		return orderActivity.createOrder(orderRequest);
	}

	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("orders")
	public Response updateOrder(OrderRequest orderRequest) {
		OrderActivity orderActivity = new OrderActivity();
		String res = orderActivity.updateOrder(orderRequest.getOrderNum(), orderRequest.getPayment());
		
		if (res.equals("STATUS UPDATED")) {
			return Response.status(Status.OK).build();
		}
		
		return null;
	}

	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("orders")
	public Response deleteOrder(@QueryParam("ordernumber") String number) {
		OrderActivity orderActivity = new OrderActivity();
		String res = orderActivity.deleteOrder(number);
		
		if (res.equals("ORDER CANCELLED")) {
			return Response.status(Status.OK).build();
		}
		
		return null;
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("orders/all")
	public Set<OrderRepresentation> getOrders() {
		OrderActivity orderActivity = new OrderActivity();
		return orderActivity.getOrders();
	}

}
