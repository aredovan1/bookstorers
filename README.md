# README #

###Bookstore ###
* This is a Bookstore Web Service that uses RESTful architecture.
* Allows users to GET a book, ADD a book, CREATE an order, UPDATE an order, GET an order and DELETE an order.
* Version 1.0


###--BOOK--###
* Accepts and returns json
* Search book by title: (GET /services/book?request=title&value={title})
* Search book by author: (GET /services/book?request=author&value={author})
* Search book by id: (GET /services/book?request=id&value={id})
* Add book: (POST /services/book)
## Test Book ##
* Search book by title: (GET /services/book?request=title&value=American%20Gods)
* Search book by author: (GET /services/book?request=author&value=Neil)
* Search book by id: (GET /services/book?request=id&value=0003)

###--CUSTOMER--###
* Accepts and returns json
* Get customer by customer ID: (GET /services/customer/{customerId})
## CUSTOMER Test ##
* Get customer by customer ID: (GET /services/customer/r1234)

###--ORDER--###
* Accepts and returns json
* Search order by order number: (GET /services/customer/{customerId}/orders/{orderNumber})
* Create an order: (POST services/customer/{customerId}/orders/)
* Update an order: (PUT services/customer/{customerId}/orders)
* Delete an order: (DELETE services/customer/{customerId}/orders/{orderNumber})
## ORDER test ##
* Search order by order number: (GET /services/customer/r1234/orders/n1111)
* Create an order: (POST services/customer/r1234/orders/)
* Update an order: (PUT services/customer/r1234/orders)
* Delete an order: (DELETE services/customer/r1234/orders/n1111)


### Creators ###
* Audrey Redovan
* Madina Igibayeva